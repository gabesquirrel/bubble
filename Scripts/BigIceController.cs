﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigIceController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        GetComponent<Animator>().SetBool("Active", true);
        lifeTimeCounter = lifeTime;
	}
	
	// Update is called once per frame
	void Update () {
        LifeTime();
	}

    public float lifeTime;
    private float lifeTimeCounter;
    private void LifeTime()
    {
        if (lifeTimeCounter > 0)
        {
            lifeTimeCounter -= Time.deltaTime;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
