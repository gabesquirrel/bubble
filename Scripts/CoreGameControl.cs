﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoreGameControl : MonoBehaviour {

    // Use this for initialization
    public GameObject bubble;//el bubble que va a ser el jugador
    public float timeToCreateBubble;//para que demore un poco de tiempo a que empiece el juego
    //-----------------------------------
    //para obtener de el el booleano de cuando ya no este activo
    public GameObject playButton;
    //--------------------------------
    public GameObject backGround;
    //--------------------------------------
    public GameObject copo;
    //-------------------------------------------

    private bool startGame;

    void Start () {
        //esto es solo para asegurarme de que no se cree mas de una bola
        //bubbleCounter = 0;//inicializo la listas de jugadres principales
        time = timeToCreateBubble; //asigno valor a otra variable para modificar y luego reestablecer el valor

        //para asignar el valor y modificarlo
        timeAtackCounter = timeAtack;

        //inicializa esta variable
        bigIceCounter = bigIceCount;
    }
	
	// Update is called once per frame
	void Update () {
        StartGame();

        if (startGame)
        {
            CreateBubble();

            //aqui puse la condicion de que si ya hay un bubble
            //para no tener que repetir la pregunta en todos los metodos quelo necesitan.
            if (bubbleClone != null)
            {
                CreateCopo();
                Walk();
                GetCopo();
                Attack();
            }
        }
    }
    
    //para que haya solo un bubble
    private static GameObject bubbleClone = null;
    //tiempo que vario para poder reestablecer luego
    private float time;
    private void CreateBubble()
    {
        //si no se ha acabado el tiempo.
        if (bubbleClone == null)//para asegurar que cuando create es false, no descuente tiempo.
        {
            //descontar del tiempo.
            time -= Time.deltaTime;
            Debug.Log(time);
        }
        //si ya se agoto el tiempo...
        if (time < 0)
        {
            bubbleClone = Instantiate(bubble);//creo a bubble
            time = timeToCreateBubble;//reseteo el tiempo
        }
    }
    private void StartGame()
    {
        if (!playButton.GetComponent<ButtonControl>().GetActive())
        {
            startGame = true;
        }

    }

    private Camera mainCamera;
    private GameObject copoClone = null;
    private void CreateCopo()
    {
        //si tocan el fondo y no esta congelado el muñeco, puede crearse un copo
        if (backGround.GetComponent<BackGroundTouchDetecter>().GetTouch() && !bubbleClone.GetComponent<Animator>().GetBool("Freezed"))
        {
            mainCamera = FindObjectOfType<Camera>();
            //Vector2 mousePoint = mainCamera.ScreenToWorldPoint(backGround.GetComponent<BackGroundTouchDetecter>().GetMousePosition());
            Vector2 mousePoint = BackGroundTouchDetecter.GetMousePosition();
            Transform t = copo.GetComponent<Transform>();
            if (copoClone == null)
            {
                //convierte la posicion del mouse segun lo ve la camara, pero luego hay que ajustar la 
                copoClone = Instantiate(copo, mousePoint, t.rotation);
            }
            else
            {
                copoClone.transform.position = mousePoint;
            }
            
            backGround.GetComponent<BackGroundTouchDetecter>().SetTouch(false);
            Debug.Log("se ha puesto false");
        }
        
    }

    //velocidad de movimiento de la burbuja
    public float bubbleSpeed;
    private void Walk()
    {
        //si esta congelado no se mueve porque
        //no se puede crear un copo 
        if (copoClone != null)
        {
            bubbleClone.GetComponent<Animator>().SetBool("Mooving", true);
            bubbleClone.transform.position = Vector2.MoveTowards(bubbleClone.transform.position, copoClone.transform.position, bubbleSpeed * Time.deltaTime);
        }
        else
        {
            bubbleClone.GetComponent<Animator>().SetBool("Mooving", false);
        }

    }

    //tenemos sueño, 
    //asi que haremos como si tuviesen la misma posicion
    //en realidad hay que hacer dos coliders
    //y cuando colisionen se absorve
    //falta desactivar el congelamiento
    private void GetCopo()
    {
        if (copoClone != null && copoClone.transform.position == bubbleClone.transform.position)
        {
            bubbleClone.GetComponent<Animator>().SetBool("Freezed", true);
            Destroy(copoClone);
            copoClone = null;
        }
    }

    public float timeAtack;//se obtiene y sepasa al valor de abajo para modificarlo
    private float timeAtackCounter;//se inicializa en Start();
    //todo lo que tenga clone es porque es de un prefab la creacion
    public GameObject bigIce;
    public float bigIceCount;
    public float bigIceCounter;
    private GameObject bigIceClone;
    public GameObject proyectile;
    private Vector2 mousePoint;
    private void Attack()
    {
        //big ice attack tiene que ser uno solo.

        //esto es para descontar tiempo de ataque general
            if (bubbleClone.GetComponent<Animator>().GetBool("Freezed") && timeAtackCounter > 0)
            {
                //que descuente tiempo.
                timeAtackCounter -= Time.deltaTime;
                //Debug.Log("descontando tiempo para atacar");

            //aqui se pone que ataque

            if (backGround.GetComponent<BackGroundTouchDetecter>().GetTouch() && bigIceClone == null && bigIceCounter > 0)
            {
                //v vector para hacer de position al hielo
                //mousePoint = mainCamera.ScreenToWorldPoint(backGround.GetComponent<BackGroundTouchDetecter>().GetMousePosition());
                mousePoint = BackGroundTouchDetecter.GetMousePosition();
                bigIceClone = Instantiate(bigIce, mousePoint, bigIce.transform.rotation);
                backGround.GetComponent<BackGroundTouchDetecter>().SetTouch(false);
                bigIceCounter -= 1;
                Debug.Log(bigIceCounter);
            }
        }
            else
            {
                timeAtackCounter = timeAtack;
                bubbleClone.GetComponent<Animator>().SetBool("Freezed", false);
                bigIceClone = null;
            bigIceCounter = bigIceCount;
        }


        //ataque con el proyectil. luego de que ataque con el hielo
        if (bigIceClone != null)
        {
            //si toco
            if (backGround.GetComponent<BackGroundTouchDetecter>().GetTouch())
            {
                //designo el mouse point para luego cogerlo desde el proyectil
                //mousePoint = mainCamera.ScreenToWorldPoint(backGround.GetComponent<BackGroundTouchDetecter>().GetMousePosition());
                mousePoint = BackGroundTouchDetecter.GetMousePosition();
                //determino su punto objetivo para que se cree con el ya designado
                //Vector2 v = mainCamera.ScreenToWorldPoint(backGround.GetComponent<BackGroundTouchDetecter>().GetMousePosition());
                //proyectile.GetComponent<ProyectilController>().SetMovePoint(v);
                //creo proyectil, le paso la direccion a al que se tiene que mover, el sabra que hacer
                GameObject p = Instantiate(proyectile, new Vector3(bubbleClone.transform.position.x, bubbleClone.transform.position.y, 0f), proyectile.transform.rotation);
                
                //hago false tocarpara que no se creen infinitos
                backGround.GetComponent<BackGroundTouchDetecter>().SetTouch(false);
            }
        }

    }
    

    //para poder manejar los objetos desde otra clase (proyectil sobre todo)
    public static GameObject GetBubble()
    {
        return bubbleClone;
    }

    public GameObject GetEnemy()
    {
        //falta poner el enmigo
        return null;
    }
    public Camera GetCamera()
    {
        return mainCamera;
    }//tiene que borrarse despues
    public GameObject GetBackGround()
    {
        return backGround;
    }//tiene que borrarse despues
    public Vector2 GetMousePoint()
    {
        return mousePoint;
    }//tiene que borrarse despues

}
