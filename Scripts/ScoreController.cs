﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreController : MonoBehaviour {

    //esta clase controlla los enemigos por oleadas.
    //la primera oleada dedos malos, la segunda dde 4 etc.
    //hasta qe no se matan a todos los de la oleada, no aparecen los de la
    //otra oleada.
    //y al cabo de determinadas oleadas (5) aparece un jefe final.
    //inicialmente el juego solo tiene a 1 jefe final
    //Si kickstarter apoya, tendrá varios, pero hay que hacer los bocetos.

    // Use this for initialization
    void Start() {
        timeCounter = spawEnemyTime;
        CallWave();//momentaneamente
    }

    // Update is called once per frame
    void Update() {
        GenerateBadBubble();
    }

    //enemigos por cada oleada
    public static float wave1 = 2;
    public static float wave2 = 4;
    public static float wave3 = 8;
    public static float wave4 = 15;
    public static float wave5 = 20;
    //------------------------
    //controla en cual oleada estas
    public static int wave = 1;
    //------------------------------------------
    //contador de enemigos a crear
    private static float enemyCounter;
    //---------------------------
    private static void CallWave()
    {
        switch (wave)
        {
            case 1: enemyCounter = wave1; break;
            case 2: enemyCounter = wave2; break;
            case 3: enemyCounter = wave3; break;
            case 4: enemyCounter = wave4; break;
            case 5: enemyCounter = wave5; break;
        }
    }

    public GameObject badBubbble;
    public float spawEnemyTime;
    private float timeCounter;
    private static float enemyOnScreen = 0;//cantidad de enemigos en la pantalla.
    private void GenerateBadBubble()
    {
        if (timeCounter < 0 && enemyCounter > 0)
        {
            Instantiate(badBubbble);
            timeCounter = spawEnemyTime;
            enemyCounter--;
            enemyOnScreen++;
        }
        else
        {
            timeCounter -= Time.deltaTime;
        }
    }

    //método llamado desde un enemigo cada vez que muere.
    private static void EnemyDead()
    {
        enemyOnScreen--;
        if (enemyOnScreen == 0 && enemyCounter == 0)
        {
            wave++;
            CallWave();
        }
    }
}
