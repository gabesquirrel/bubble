﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ButtonControl : MonoBehaviour {

    public bool active;//indica si esta activo este boton listo para pulsarse
    public bool GetActive()
    {
        return active;
    }

	// Use this for initialization
	void Start () {
        active = true;
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        Reboot();
	}
    
    Animator anim;

    private void OnMouseDown()
    {
        if (!anim.GetBool("Out"))
        {
            anim.SetBool("Out", true);
            active = false;
        }
            
    }
    private void Reboot()
    {
        if (active)
        {
            //desactiva out y se reinici la animacion.
            anim.SetBool("Out", false);
        }
    }
}
