﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BubbleGameController
{
    public class ProyectilController : MonoBehaviour
    {
        //necesita la referencia del jugador y de los enemigos
        //(Clones obtenidos del core game)
        private GameObject bubble;
        private GameObject enemy;
        private  Vector3 direction;
        // Use this for initialization
        void Start()
        {
            //bubble = coreGame.GetComponent<CoreGameControl>().GetBubble();
            //enemy = coreGame.GetComponent<CoreGameControl>().GetEnemy();
            //GetComponent<Transform>().position = new Vector2(bubble.GetComponent<Transform>().position.x, bubble.GetComponent<Transform>().position.y);
            //movePoint = coreGame.GetComponent<CoreGameControl>().GetCamera().ScreenToWorldPoint(coreGame.GetComponent<CoreGameControl>().GetBackGround().GetComponent<BackGroundTouchDetecter>().GetMousePosition());
            movePoint = BackGroundTouchDetecter.GetMousePosition();
            direction = (movePoint - transform.position).normalized;
        }

        // Update is called once per frame
        void Update()
        {
            Move();
        }

        //movePoint es la posicion del dedo cuando se cra el proyectil
        //hay que moverse en esa dirección hasta salirse de la pantalla...
        //...o  chocar con enemigo
        public float speed;//velocidad a al que se mueve el proyectil
        public float time;
        private Vector3 movePoint;
        private void Move()
        {
            GetComponent<Transform>().position += direction * speed * Time.deltaTime;
            //GetComponent<Transform>().Translate(movePoint * Time.deltaTime * speed);
            //GetComponent<Transform>().position = Vector2.MoveTowards(GetComponent<Transform>().position, movePoint, Time.deltaTime * speed);
        }
    }
}
