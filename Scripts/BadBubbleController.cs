﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BadBubbleController : MonoBehaviour {

	// Use this for initialization
	void Start () {
        transform.position = new Vector3(Random.Range(-3f, 3f), Random.Range(-5f, 5f), 0f);
	}
	
	// Update is called once per frame
	void Update () {
        Move();
	}

    public float speed;
    private void Move()
    {
        transform.position = Vector2.MoveTowards(transform.position, CoreGameControl.GetBubble().transform.position, speed * Time.deltaTime);
    }
}
